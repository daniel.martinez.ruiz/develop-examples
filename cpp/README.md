# C++

## Steps on CI

* [ ] Format - `clang-format`
* [X] Build - `gcc / cmake`
* [X] Static analysis - `cppcheck / clang-tidy`
* [ ] Dynamic analysis
  * [X] Unit testing - `gtest / gmock`
  * [X] Coverage - `gcovr`
  * [ ] Functional testing - `valgrind`
