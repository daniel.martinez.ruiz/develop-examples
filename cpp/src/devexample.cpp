#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <stdexcept>

#include "devexample.hpp"
#include "minicalc.hpp"

// cppcheck-suppress passedByValue
DevExample::DevExample(std::unique_ptr<IMiniCalc> mc)
    : minicalc(std::move(mc))
{
}

void DevExample::usage(std::string &name)
{
    std::cout << "usage: " << name << " [-h] {factorial,gcd} num [num ...]" << std::endl
        << std::endl
        << "Maiarhawkin devexample minicalculator." << std::endl
        << std::endl
        << "positional arguments:" << std::endl
        << "  {factorial,gcd}  operation" << std::endl
        << "  num              numbers to operate with" << std::endl
        << std::endl
        << "optional arguments:" << std::endl
        << "  -h, --help       show this help message and exit" << std::endl;
}

int DevExample::run(int argc, char* argv[])
{
    std::string program_name = argv[0];
    if (argc < 3)
    {
        DevExample::usage(program_name);
        return 2;
    }

    for (int i = 1; i < argc; ++i)
    {
        std::string arg = argv[i];
        if ((arg == "-h") || (arg == "--help"))
        {
            DevExample::usage(program_name);
            return 1;
        }
    }

    try
    {
        int result = 0;
        std::string operation = argv[1];
        if (operation == "factorial")
        {
            result = minicalc->factorial(std::stoi(argv[2]));
        }
        else if (operation == "gcd")
        {
            std::vector<int> nums;
            for (int i = 2; i < argc; ++i)
            {
                nums.push_back(std::stoi(argv[i]));
            }
            result = minicalc->gcd(nums);
        }
        else
        {
            DevExample::usage(program_name);
            return 2;
        }

        std::cout << result << std::endl;

    }
    catch (std::invalid_argument &err)
    {
        std::cout << "Invalid argument numbers" << std::endl;
        return 1;
    }

    return 0;
}
