#include <stdexcept>

#include "minicalc.hpp"

int MiniCalc::factorial(int num)
{
    if (num < 0)
    {
        throw std::invalid_argument("received negative value");
    }
    else if (num < 2)
    {
        return 1;
    }
    else
    {
        return num * factorial(num - 1);
    }
}

int MiniCalc::gcd2(int num_a, int num_b)
{
    if (num_b == 0)
    {
        return num_a;
    }
    else
    {
        return gcd2(num_b, num_a % num_b);
    }
}

int MiniCalc::gcd(std::vector<int> arr)
{
    if (arr.size() == 0)
    {
        return 0;
    }
    else
    {
        int left = arr[0];
        for (unsigned int i = 1; i < arr.size(); ++i)
        {
            left = gcd2(left, arr[i]);
        }

        return left;
    }
}
