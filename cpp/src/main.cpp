#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <stdexcept>

#include "devexample.hpp"

int main(int argc, char* argv[])
{
    std::unique_ptr<MiniCalc> minicalc(new MiniCalc);
    DevExample devexample(std::move(minicalc));
    return devexample.run(argc, argv);
}
