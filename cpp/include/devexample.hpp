#ifndef DEVEXAMPLE_HPP
#define DEVEXAMPLE_HPP

#include <string>
#include <memory>

#include "minicalc.hpp"

class DevExample
{
public:
    explicit DevExample(std::unique_ptr<IMiniCalc> mc);

    static void usage(std::string &name);
    int run(int argc, char* argv[]);

private:
    std::unique_ptr<IMiniCalc> minicalc;

};

#endif // DEVEXAMPLE_HPP
