#ifndef MINICALC_HPP
#define MINICALC_HPP

#include <vector>

#include "iminicalc.hpp"

class MiniCalc : public IMiniCalc
{

public:
    ~MiniCalc() {};

    int factorial(int num) override;
    int gcd2(int num_a, int num_b) override;
    int gcd(std::vector<int> arr) override;

};

#endif // MINICALC_HPP
