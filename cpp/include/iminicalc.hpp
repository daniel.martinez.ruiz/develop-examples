#ifndef IMINICALC_HPP
#define IMINICALC_HPP

#include <vector>

class IMiniCalc
{

public:
    virtual ~IMiniCalc() {};

    virtual int factorial(int num) = 0;
    virtual int gcd2(int num_a, int num_b) = 0;
    virtual int gcd(std::vector<int> arr) = 0;

};

#endif // IMINICALC_HPP
