#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "devexample.hpp"
#include "minicalc.hpp"
#include <string>
#include <vector>
#include <memory>
#include <stdexcept>

namespace {

class MockMiniCalc : public IMiniCalc
{

public:
    MOCK_METHOD1(factorial, int(int num));
    MOCK_METHOD2(gcd2, int(int num_a, int num_b));
    MOCK_METHOD1(gcd, int(std::vector<int> arr));
};

TEST(TestDevExample, run)
{
    std::unique_ptr<MockMiniCalc> mocked_minicalc(new MockMiniCalc);
    EXPECT_CALL(*mocked_minicalc, factorial(-1))
        .WillRepeatedly(::testing::Throw(std::invalid_argument("received negative value")));
    EXPECT_CALL(*mocked_minicalc, factorial(1))
        .WillRepeatedly(::testing::Return(1));
    EXPECT_CALL(*mocked_minicalc, factorial(3))
        .WillRepeatedly(::testing::Return(6));
    EXPECT_CALL(*mocked_minicalc, gcd(std::vector<int> { 1 }))
        .WillRepeatedly(::testing::Return(1));
    EXPECT_CALL(*mocked_minicalc, gcd(std::vector<int> { 1, 2 }))
        .WillRepeatedly(::testing::Return(1));
    EXPECT_CALL(*mocked_minicalc, gcd(std::vector<int> { 24, 18 }))
        .WillRepeatedly(::testing::Return(6));
    DevExample devexample(std::move(mocked_minicalc));


    char PROGRAM_NAME[] = "dev-example_test";
    char PARAM_HELP[] = "-h";
    char PARAM_HELP2[] = "--help";
    char OP_FACTORIAL[] = "factorial";
    char OP_GCD[] = "gcd";
    char OP_OTHER[] = "other";
    char VALUE__1[] = "-1";
    char VALUE_1[] = "1";
    char VALUE_2[] = "2";
    char VALUE_3[] = "3";
    char VALUE_18[] = "18";
    char VALUE_24[] = "24";

    char* argv0[] = { PROGRAM_NAME };
    ::testing::internal::CaptureStdout();
    EXPECT_EQ(devexample.run(1, argv0), 2);
    ::testing::internal::GetCapturedStdout();

    char* argv1[] = { PROGRAM_NAME, OP_FACTORIAL, PARAM_HELP };
    ::testing::internal::CaptureStdout();
    EXPECT_EQ(devexample.run(3, argv1), 1);
    ::testing::internal::GetCapturedStdout();

    char* argv1_2[] = { PROGRAM_NAME, OP_FACTORIAL, PARAM_HELP2 };
    ::testing::internal::CaptureStdout();
    EXPECT_EQ(devexample.run(3, argv1_2), 1);
    ::testing::internal::GetCapturedStdout();

    char* argv2[] = { PROGRAM_NAME, OP_FACTORIAL };
    ::testing::internal::CaptureStdout();
    EXPECT_EQ(devexample.run(2, argv2), 2);
    ::testing::internal::GetCapturedStdout();

    char* argv3[] = { PROGRAM_NAME, OP_GCD };
    ::testing::internal::CaptureStdout();
    EXPECT_EQ(devexample.run(2, argv3), 2);
    ::testing::internal::GetCapturedStdout();

    char* argv3_2[] = { PROGRAM_NAME, OP_OTHER, VALUE_1 };
    ::testing::internal::CaptureStdout();
    EXPECT_EQ(devexample.run(3, argv3_2), 2);
    ::testing::internal::GetCapturedStdout();

    char* argv4[] = { PROGRAM_NAME, OP_FACTORIAL, VALUE__1 };
    ::testing::internal::CaptureStdout();
    EXPECT_EQ(devexample.run(3, argv4), 1);
    ::testing::internal::GetCapturedStdout();

    char* argv5[] = { PROGRAM_NAME, OP_FACTORIAL, VALUE_1 };
    ::testing::internal::CaptureStdout();
    EXPECT_EQ(devexample.run(3, argv5), 0);
    EXPECT_EQ(std::stoi(::testing::internal::GetCapturedStdout()), 1);

    char* argv6[] = { PROGRAM_NAME, OP_FACTORIAL, VALUE_3 };
    ::testing::internal::CaptureStdout();
    EXPECT_EQ(devexample.run(3, argv6), 0);
    EXPECT_EQ(std::stoi(::testing::internal::GetCapturedStdout()), 6);

    char* argv7[] = { PROGRAM_NAME, OP_GCD, VALUE_1 };
    ::testing::internal::CaptureStdout();
    EXPECT_EQ(devexample.run(3, argv7), 0);
    EXPECT_EQ(std::stoi(::testing::internal::GetCapturedStdout()), 1);

    char* argv8[] = { PROGRAM_NAME, OP_GCD, VALUE_1, VALUE_2 };
    ::testing::internal::CaptureStdout();
    EXPECT_EQ(devexample.run(4, argv8), 0);
    EXPECT_EQ(std::stoi(::testing::internal::GetCapturedStdout()), 1);

    char* argv9[] = { PROGRAM_NAME, OP_GCD, VALUE_24, VALUE_18 };
    ::testing::internal::CaptureStdout();
    EXPECT_EQ(devexample.run(4, argv9), 0);
    EXPECT_EQ(std::stoi(::testing::internal::GetCapturedStdout()), 6);

}

} // namespace
