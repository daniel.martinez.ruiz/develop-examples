#include "gtest/gtest.h"

#include "minicalc.hpp"
#include <vector>

namespace {

class TestMiniCalc : public ::testing::Test {

protected:
    MiniCalc minicalc;

protected:

    void SetUp() override {

    }
};

TEST_F(TestMiniCalc, factorial)
{
    EXPECT_THROW(minicalc.factorial(-1), std::invalid_argument);
    EXPECT_EQ(minicalc.factorial(0), 1);
    EXPECT_EQ(minicalc.factorial(1), 1);
    EXPECT_EQ(minicalc.factorial(2), 2);
    EXPECT_EQ(minicalc.factorial(3), 6);
    EXPECT_EQ(minicalc.factorial(5), 120);
    EXPECT_EQ(minicalc.factorial(10), 3628800);
}

TEST_F(TestMiniCalc, gcd2)
{
    EXPECT_EQ(minicalc.gcd2(0, 0), 0);
    EXPECT_EQ(minicalc.gcd2(1, 1), 1);
    EXPECT_EQ(minicalc.gcd2(2, 2), 2);
    EXPECT_EQ(minicalc.gcd2(2, 3), 1);
    EXPECT_EQ(minicalc.gcd2(2, 4), 2);
    EXPECT_EQ(minicalc.gcd2(36, 60), 12);
}

TEST_F(TestMiniCalc, gcd)
{
    std::vector<int> test1;
    EXPECT_EQ(minicalc.gcd(test1), 0);

    int integers2[] = { 2, 3, 4, 5, 6 };
    std::vector<int> test2(integers2, integers2 + 5);
    EXPECT_EQ(minicalc.gcd(test2), 1);

    int integers3[] = { 2, 4, 6, 8, 10 };
    std::vector<int> test3(integers3, integers3 + 5);
    EXPECT_EQ(minicalc.gcd(test3), 2);
}

} // namespace