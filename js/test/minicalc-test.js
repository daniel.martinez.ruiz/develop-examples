const expect = require('chai').expect;
const MiniCalc = require('../src/minicalc');

describe('MiniCalc', function() {
  describe('factorial()', function() {
    const minicalc = new MiniCalc();

    it('factorial(-1) should throw an exception', function() {
      expect(() => minicalc.factorial(-1)).to.throw('received negative value');
    });

    testValues = [
      [0, 1],
      [1, 1],
      [2, 2],
      [3, 6],
      [5, 120],
      [10, 3628800],
    ];

    testValues.forEach((element) => {
      it(
          'factorial(' + element[0] + ') should return ' + element[1],
          function() {
            expect(minicalc.factorial(element[0])).to.equal(element[1]);
          }
      );
    });
  });

  describe('gcd2()', function() {
    const minicalc = new MiniCalc();

    testValues = [
      [0, 0, 0],
      [1, 1, 1],
      [2, 2, 2],
      [2, 3, 1],
      [2, 4, 2],
      [36, 60, 12],
    ];

    testValues.forEach((element) => {
      it(
          'gcd2(' + element[0] + ', ' + element[1] + ') should return ' +
          element[2],
          function() {
            expect(minicalc.gcd2(element[0], element[1])).to.equal(element[2]);
          }
      );
    });
  });

  describe('gcd()', function() {
    const minicalc = new MiniCalc();

    testValues = [
      [[], 0],
      [[2, 3, 4, 5, 6], 1],
      [[2, 4, 6, 8, 10], 2],
    ];

    testValues.forEach((element) => {
      it('gcd(' + element[0] + ') should return ' + element[1], function() {
        expect(minicalc.gcd(element[0])).to.equal(element[1]);
      });
    });
  });
});
