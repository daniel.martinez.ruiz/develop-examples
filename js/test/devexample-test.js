const expect = require('chai').expect;
const sinon = require('sinon');
const DevExample = require('../src/devexample');

describe('DevExample', function() {
  describe('run()', function() {
    const PROGRAM_NAME = 'dev-example';
    let output;
    const write = process.stdout.write;
    const devexample = new DevExample();

    before(function() {
      sinon.stub(devexample.minicalc, 'factorial')
          .withArgs(-1).throwsException('received negative value')
          .withArgs(1).returns(1)
          .withArgs(3).returns(6);
      sinon.stub(devexample.minicalc, 'gcd')
          .withArgs([1]).returns(1)
          .withArgs([1, 2]).returns(1)
          .withArgs([24, 18]).returns(6);
    });

    getOutput = function(str) {
      output += str;
    };

    beforeEach(function() {
      output = '';
      process.stdout.write = getOutput;
    });

    afterEach(function() {
      process.stdout.write = write;
    });

    after(function() {
      process.stdout.write = write;
      devexample.minicalc.factorial.restore();
      devexample.minicalc.gcd.restore();
    });

    it('without args should return code 2', function() {
      const ret = devexample.run([PROGRAM_NAME]);
      process.stdout.write = write;
      expect(ret).to.equal(2);
    });
    it('with -h param should return code 1', function() {
      const ret = devexample.run([PROGRAM_NAME, 'factorial', '-h']);
      process.stdout.write = write;
      expect(ret).to.equal(1);
    });
    it('with --help param should return code 1', function() {
      const ret = devexample.run([PROGRAM_NAME, 'factorial', '--help']);
      process.stdout.write = write;
      expect(ret).to.equal(1);
    });
    it('with only operation factorial should return code 2', function() {
      const ret = devexample.run([PROGRAM_NAME, 'factorial']);
      process.stdout.write = write;
      expect(ret).to.equal(2);
    });
    it('with only operation gcd should return code 2', function() {
      const ret = devexample.run([PROGRAM_NAME, 'gcd']);
      process.stdout.write = write;
      expect(ret).to.equal(2);
    });
    it('other 1 should return code 2', function() {
      const ret = devexample.run([PROGRAM_NAME, 'other', '1']);
      process.stdout.write = write;
      expect(ret).to.equal(2);
    });

    it('factorial -1 should return code 1', function() {
      const ret = devexample.run([PROGRAM_NAME, 'factorial', '-1']);
      process.stdout.write = write;
      expect(ret).to.equal(1);
    });

    it('factorial 1 should return 1 and code 0', function() {
      const ret = devexample.run([PROGRAM_NAME, 'factorial', '1']);
      process.stdout.write = write;
      expect(ret).to.equal(0);
      expect(output.trim()).to.equal('1');
    });
    it('factorial 3 should return 6 and code 0', function() {
      const ret = devexample.run([PROGRAM_NAME, 'factorial', '3']);
      process.stdout.write = write;
      expect(ret).to.equal(0);
      expect(output.trim()).to.equal('6');
    });

    it('gcd 1 should return 1 and code 0', function() {
      const ret = devexample.run([PROGRAM_NAME, 'gcd', '1']);
      process.stdout.write = write;
      expect(ret).to.equal(0);
      expect(output.trim()).to.equal('1');
    });
    it('gcd 1 2 should return 1 and code 0', function() {
      const ret = devexample.run([PROGRAM_NAME, 'gcd', '1', '2']);
      process.stdout.write = write;
      expect(ret).to.equal(0);
      expect(output.trim()).to.equal('1');
    });
    it('gcd 24 18 should return 1 and code 0', function() {
      const ret = devexample.run([PROGRAM_NAME, 'gcd', '24', '18']);
      process.stdout.write = write;
      expect(ret).to.equal(0);
      expect(output.trim()).to.equal('6');
    });
  });
});
