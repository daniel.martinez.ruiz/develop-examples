const DevExample = require('../src/devexample');
const devexample = new DevExample();

process.argv.shift();
return devexample.run(process.argv);
