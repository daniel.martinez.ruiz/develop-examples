const MiniCalc = require('./minicalc');

/**
 * Parse CLI arguments and execute the minicalculator.
 */
class DevExample {
  /**
   * @constructor
   */
  constructor() {
    this.minicalc = new MiniCalc();
  }

  /**
   * Calculates the factorial.
   * @param {string} name The program name.
   */
  usage(name) {
    process.stdout.write(
        'usage: ' + name + ' [-h] {factorial,gcd} num [num ...]\n' +
        '\n' +
        'Maiarhawkin devexample minicalculator.\n' +
        '\n' +
        'positional arguments:\n' +
        '  {factorial,gcd}  operation\n' +
        '  num              numbers to operate with\n' +
        '\n' +
        'optional arguments:\n' +
        '  -h, --help       show this help message and exit\n');
  }

  /**
   * Run the parser and execute the minicalc.
   * @param {array} args The arguments.
   * @return {number} The error status code.
   */
  run(args) {
    const programName = args[0];
    if (args.length < 3) {
      this.usage(programName);
      return 2;
    }

    for (let i = 1; i < args.length; ++i) {
      const arg = args[i];
      if ((arg == '-h') || (arg == '--help')) {
        this.usage(programName);
        return 1;
      }
    }

    try {
      let result = 0;
      const operation = args[1];
      if (operation == 'factorial') {
        result = this.minicalc.factorial(parseInt(args[2]));
      } else if (operation == 'gcd') {
        const nums = [];
        for (let i = 2; i < args.length; ++i) {
          nums.push(parseInt(args[i]));
        }
        result = this.minicalc.gcd(nums);
      } else {
        this.usage(programName);
        return 2;
      }

      process.stdout.write(result.toString() + '\n');
    } catch (err) {
      process.stdout.write('Invalid argument numbers\n');
      return 1;
    }

    return 0;
  }
}

module.exports = DevExample;
