/**
 * Represents a mini-calculator.
 */
class MiniCalc {
  /**
   * @constructor
   */
  constructor() {}

  /**
   * Calculates the factorial.
   * @param {number} num The number.
   * @return {number} The factorial of the number.
   */
  factorial(num) {
    if (num < 0) {
      throw new Error('received negative value');
    } else if (num < 2) {
      return 1;
    } else {
      return num * this.factorial(num - 1);
    }
  }

  /**
   * Calculates the Greatest Common Divisor of two numbers.
   * @param {number} numA First number.
   * @param {number} numB Second number.
   * @return {number} The GCD of the two numbers.
   */
  gcd2(numA, numB) {
    if (numB == 0) {
      return numA;
    } else {
      return this.gcd2(numB, numA % numB);
    }
  }

  /**
   * Calculates the Greatest Common Divisor of an array of numbers.
   * @param {array} arr The array of numbers.
   * @return {number} The GCD of all numbers.
   */
  gcd(arr) {
    if (arr.length == 0) {
      return 0;
    } else {
      let left = arr[0];
      for (let i = 1; i < arr.length; ++i) {
        left = this.gcd2(left, arr[i]);
      }
      return left;
    }
  }
}

module.exports = MiniCalc;
