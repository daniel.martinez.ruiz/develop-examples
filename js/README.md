# JS

## Steps on CI

* [ ] Format - `eslint`
* [ ] Build
* [X] Static analysis - `eslint`
* [ ] Dynamic analysis
  * [X] Unit testing - `mocha / chai / sinon`
  * [X] Coverage - `nyc`
  * [ ] Functional testing
