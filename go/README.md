# GoLang

## Steps on CI

* [ ] Format - ``
* [ ] Build - `go build`
* [X] Static analysis - `golint`
* [ ] Dynamic analysis
  * [X] Unit testing - `go test`
  * [X] Coverage - `go tool cover`
  * [ ] Functional testing
