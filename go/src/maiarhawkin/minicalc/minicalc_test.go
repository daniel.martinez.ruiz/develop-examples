package minicalc

import "testing"

func TestFactorialException(t *testing.T) {
	m := Minicalc{}
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("factoral(-1) does not return a panic")
		}
	}()
	m.Factorial(-1)
}

func TestFactorial(t *testing.T) {
	m := Minicalc{}
	cases := []struct {
		in, want int
	}{
		{0, 1},
		{1, 1},
		{2, 2},
		{3, 6},
		{5, 120},
		{10, 3628800},
	}
	for _, c := range cases {
		got := m.Factorial(c.in)
		if got != c.want {
			t.Errorf("Factorial(%d) == %d, want %d", c.in, got, c.want)
		}
	}
}

func TestGcd2(t *testing.T) {
	m := Minicalc{}
	cases := []struct {
		inA, inB, want int
	}{
		{0, 0, 0},
		{1, 1, 1},
		{2, 2, 2},
		{2, 3, 1},
		{2, 4, 2},
		{36, 60, 12},
	}
	for _, c := range cases {
		got := m.Gcd2(c.inA, c.inB)
		if got != c.want {
			t.Errorf("Gcd2(%d, %d) == %d, want %d", c.inA, c.inB, got, c.want)
		}
	}
}

func TestGcd(t *testing.T) {
	m := Minicalc{}
	cases := []struct {
		in   []int
		want int
	}{
		{[]int{}, 0},
		{[]int{2, 3, 4, 5, 6}, 1},
		{[]int{2, 4, 6, 8, 10}, 2},
	}
	for _, c := range cases {
		got := m.Gcd(c.in)
		if got != c.want {
			t.Errorf("Gcd(%d) == %d, want %d", c.in, got, c.want)
		}
	}
}
