package minicalc

// Operations is the named interface
type Operations interface {
	Factorial(num int) int
	Gcd2(numA, numB int) int
	Gcd(arr []int) int
}

// Minicalc implementation of the operations.
type Minicalc struct {
}

// Factorial function.
func (m Minicalc) Factorial(num int) int {
	if num < 0 {
		panic("received negative value")
	} else if num < 2 {
		return 1
	} else {
		return num * m.Factorial(num-1)
	}
}

// Gcd2 is the Greater Common Divisor of 2 numbers.
func (m Minicalc) Gcd2(numA, numB int) int {
	if numB == 0 {
		return numA
	}

	return m.Gcd2(numB, numA%numB)
}

// Gcd is the Greater Common Divisor of all numbers in an array.
func (m Minicalc) Gcd(arr []int) int {
	if len(arr) == 0 {
		return 0
	}

	left := arr[0]
	for i := 1; i < len(arr); i++ {
		left = m.Gcd2(left, arr[i])
	}

	return left
}
