package main

import (
	"fmt"
	"maiarhawkin/minicalc"
	"path"
	"strconv"
)

func usage(name string) {
	fmt.Println("usage: " + name + " [-h] {factorial,gcd} num [num ...]")
	fmt.Println()
	fmt.Println("Maiarhawkin devexample minicalculator.")
	fmt.Println()
	fmt.Println("positional arguments:")
	fmt.Println("  {factorial,gcd}  operation")
	fmt.Println("  num              numbers to operate with")
	fmt.Println()
	fmt.Println("optional arguments:")
	fmt.Println("  -h, --help       show this help message and exit")
}

func run(m minicalc.Operations, args []string) (result int) {
	programName := path.Base(args[0])
	if len(args) < 3 {
		usage(programName)
		return 2
	}

	for _, arg := range args {
		if arg == "-h" || arg == "--help" {
			usage(programName)
			return 1
		}
	}

	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Invalid argument numbers")
			result = 1
		}
	}()

	response := 0
	operation := args[1]
	if operation == "factorial" {
		arg, _ := strconv.Atoi(args[2])
		response = m.Factorial(arg)
	} else if operation == "gcd" {
		nums := make([]int, len(args[2:]))
		for i, element := range args[2:] {
			arg, _ := strconv.Atoi(element)
			nums[i] = arg
		}
		response = m.Gcd(nums)
	} else {
		usage(programName)
		return 2
	}

	fmt.Println(response)

	return 0
}
