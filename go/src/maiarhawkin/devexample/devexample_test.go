package main

import (
	"bytes"
	"io"
	mock_minicalc "mocks"
	"os"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
)

func raisePanic(num int) int {
	panic("received negative value")
}

func TestMain(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	mockObj := mock_minicalc.NewMockOperations(mockCtrl)
	mockObj.EXPECT().Factorial(gomock.Eq(-1)).Do(raisePanic).AnyTimes()
	mockObj.EXPECT().Factorial(gomock.Eq(1)).Return(1).AnyTimes()
	mockObj.EXPECT().Factorial(gomock.Eq(3)).Return(6).AnyTimes()
	mockObj.EXPECT().Gcd(gomock.Eq([]int{1})).Return(1).AnyTimes()
	mockObj.EXPECT().Gcd(gomock.Eq([]int{1, 2})).Return(1).AnyTimes()
	mockObj.EXPECT().Gcd(gomock.Eq([]int{24, 18})).Return(6).AnyTimes()

	stdout := os.Stdout
	outC := make(chan string)

	cases := []struct {
		in   []string
		code int
		want string
	}{
		{[]string{"devexample"}, 2, ""},
		{[]string{"devexample", "factorial", "-h"}, 1, ""},
		{[]string{"devexample", "factorial", "--help"}, 1, ""},
		{[]string{"devexample", "factorial"}, 2, ""},
		{[]string{"devexample", "gcd"}, 2, ""},
		{[]string{"devexample", "other", "1"}, 2, ""},
		{[]string{"devexample", "factorial", "-1"}, 1, ""},
		{[]string{"devexample", "factorial", "1"}, 0, "1"},
		{[]string{"devexample", "factorial", "3"}, 0, "6"},
		{[]string{"devexample", "gcd", "1"}, 0, "1"},
		{[]string{"devexample", "gcd", "1", "2"}, 0, "1"},
		{[]string{"devexample", "gcd", "24", "18"}, 0, "6"},
	}
	for _, c := range cases {
		r, w, _ := os.Pipe()
		os.Stdout = w

		gotCode := run(mockObj, c.in)
		gotResult := ""
		if gotCode != c.code {
			t.Errorf("Calling with params %q returns code %d, want %d", strings.Join(c.in[1:], " "), gotCode, c.code)
			w.Close()
		} else {
			go func() {
				var buf bytes.Buffer
				io.Copy(&buf, r)
				outC <- buf.String()
			}()

			w.Close()
			gotResult = <-outC
		}

		gotResult = strings.TrimRight(gotResult, "\n")
		if gotCode == 0 && gotResult != c.want {
			t.Errorf("Calling with params %q returns %q, want %q", strings.Join(c.in[1:], " "), gotResult, c.want)
		}
	}

	os.Stdout = stdout
}
