package main

import (
	"maiarhawkin/minicalc"
	"os"
)

func main() {
	m := minicalc.Minicalc{}
	os.Exit(run(m, os.Args))
}
