<?php

interface IMiniCalc
{
    public function factorial($num): int;
    public function gcd2($num_a, $num_b): int;
    public function gcd($arr): int;
}

?>