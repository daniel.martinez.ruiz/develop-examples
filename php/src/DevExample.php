<?php

include "MiniCalc.php";

class DevExample
{
    protected $minicalc;

    function __construct()
    {
        $this->minicalc = new MiniCalc();
    }

    private function usage($name): void
    {
        print "usage: ".$name." [-h] {factorial,gcd} num [num ...]".PHP_EOL
            .PHP_EOL
            ."Maiarhawkin devexample minicalculator.".PHP_EOL
            .PHP_EOL
            ."positional arguments:".PHP_EOL
            ."  {factorial,gcd}  operation".PHP_EOL
            ."  num              numbers to operate with".PHP_EOL
            .PHP_EOL
            ."optional arguments:".PHP_EOL
            ."  -h, --help       show this help message and exit".PHP_EOL;
    }

    public function run($args): int
    {
        $program_name = $args[0];
        if (count($args) < 3)
        {
            $this->usage($program_name);
            return 2;
        }

        for ($i = 1; $i < count($args); ++$i)
        {
            $arg = $args[$i];
            if (($arg == "-h") || ($arg == "--help"))
            {
                $this->usage($program_name);
                return 1;
            }
        }

        try
        {
            $result = 0;
            $operation = $args[1];
            if ($operation == "factorial")
            {
                $result = $this->minicalc->factorial($args[2]);
            }
            else if ($operation == "gcd")
            {
                $nums = array();
                for ($i = 2; $i < count($args); ++$i)
                {
                    $nums[] = $args[$i];
                }
                $result = $this->minicalc->gcd($nums);
            }
            else
            {
                $this->usage($program_name);
                return 2;
            }

            print $result.PHP_EOL;

        }
        catch (Exception $err)
        {
            print "Invalid argument numbers".PHP_EOL;
            return 1;
        }

        return 0;
    }
}