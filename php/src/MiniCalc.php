<?php

include "IMiniCalc.php";

class MiniCalc implements IMiniCalc
{
    public function factorial($num): int
    {
        if ($num < 0)
        {
            throw new InvalidArgumentException("received negative value");
        }
        else if ($num < 2)
        {
            return 1;
        }
        else
        {
            return $num * $this->factorial($num - 1);
        }
    }

    public function gcd2($num_a, $num_b): int
    {
        if ($num_b == 0)
        {
            return $num_a;
        }
        else
        {
            return $this->gcd2($num_b, $num_a % $num_b);
        }
    }

    public function gcd($arr): int
    {
        if (count($arr) == 0)
        {
            return 0;
        }
        else
        {
            $left = $arr[0];
            for ($i = 1; $i < count($arr); ++$i)
            {
                $left = $this->gcd2($left, $arr[$i]);
            }

            return $left;
        }
    }

}

?>