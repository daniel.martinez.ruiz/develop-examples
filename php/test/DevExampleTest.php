<?php
declare(strict_types=1);

include_once "src/DevExample.php";

use PHPUnit\Framework\TestCase;

final class DevExampleTest extends TestCase
{
    protected $devexample;

    protected function setUp(): void
    {
        $this->devexample = new DevExample();
        $this->setOutputCallback(function() {});
    }

    public function testDevExampleRunException()
    {
        $mock_minicalc = $this->createMock(MiniCalc::class);
        $mock_minicalc->method('factorial')->will($this->throwException(new InvalidArgumentException("received negative value")));
        $this->assertEquals($this->devexample->run(array("dev-example_test", "factorial", -1)), 1);
    }

    public function testDevExampleRun(): void
    {
        $mock_minicalc = $this->createMock(MiniCalc::class);
        $map_factorial = [
            [ 1, 1 ],
            [ 3, 6 ]
        ];
        $mock_minicalc->method('factorial')->will($this->returnValueMap($map_factorial));
        $map_gcd = [
            [ 1, 1 ],
            [ 1, 2, 1 ],
            [ 24, 18, 6 ]
        ];
        $mock_minicalc->method('gcd')->will($this->returnValueMap($map_gcd));

        $this->assertEquals($this->devexample->run(array("dev-example_test")), 2);
        $this->assertEquals($this->devexample->run(array("dev-example_test", "factorial", "-h")), 1);
        $this->assertEquals($this->devexample->run(array("dev-example_test", "factorial", "--help")), 1);
        $this->assertEquals($this->devexample->run(array("dev-example_test", "factorial")), 2);
        $this->assertEquals($this->devexample->run(array("dev-example_test", "gcd")), 2);
        $this->assertEquals($this->devexample->run(array("dev-example_test", "other", 1)), 2);

        ob_clean();
        $this->assertEquals($this->devexample->run(array("dev-example_test", "factorial", 1)), 0);
        $this->assertEquals(trim($this->getActualOutput()), "1");
        ob_clean();
        $this->assertEquals($this->devexample->run(array("dev-example_test", "factorial", 3)), 0);
        $this->assertEquals(trim($this->getActualOutput()), "6");
        ob_clean();
        $this->assertEquals($this->devexample->run(array("dev-example_test", "gcd", 1)), 0);
        $this->assertEquals(trim($this->getActualOutput()), "1");
        ob_clean();
        $this->assertEquals($this->devexample->run(array("dev-example_test", "gcd", 1, 2)), 0);
        $this->assertEquals(trim($this->getActualOutput()), "1");
        ob_clean();
        $this->assertEquals($this->devexample->run(array("dev-example_test", "gcd", 24, 18)), 0);
        $this->assertEquals(trim($this->getActualOutput()), "6");
    }

}
