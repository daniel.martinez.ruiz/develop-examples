<?php
declare(strict_types=1);

include_once "src/MiniCalc.php";

use PHPUnit\Framework\TestCase;

final class MiniCalcTest extends TestCase
{
    protected $minicalc;

    protected function setUp(): void
    {
        $this->minicalc = new MiniCalc();
    }

    public function testMiniCalcFactorialException(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $this->minicalc->factorial(-1);
    }

    public function testMiniCalcFactorial(): void
    {
        $this->assertEquals($this->minicalc->factorial(0), 1);
        $this->assertEquals($this->minicalc->factorial(1), 1);
        $this->assertEquals($this->minicalc->factorial(2), 2);
        $this->assertEquals($this->minicalc->factorial(3), 6);
        $this->assertEquals($this->minicalc->factorial(5), 120);
        $this->assertEquals($this->minicalc->factorial(10), 3628800);
    }

    public function testMiniCalcGcd2(): void
    {
        $this->assertEquals($this->minicalc->gcd2(0, 0), 0);
        $this->assertEquals($this->minicalc->gcd2(1, 1), 1);
        $this->assertEquals($this->minicalc->gcd2(2, 2), 2);
        $this->assertEquals($this->minicalc->gcd2(2, 3), 1);
        $this->assertEquals($this->minicalc->gcd2(2, 4), 2);
        $this->assertEquals($this->minicalc->gcd2(36, 60), 12);
    }

    public function testMiniCalcGcd(): void
    {
        $this->assertEquals($this->minicalc->gcd(array()), 0);
        $this->assertEquals($this->minicalc->gcd(array(2, 3, 4, 5, 6)), 1);
        $this->assertEquals($this->minicalc->gcd(array(2, 4, 6, 8, 10)), 2);
    }
}
?>