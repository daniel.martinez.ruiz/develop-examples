# develop-examples

* [ ] Format - ``
* [ ] Build - ``
* [X] Static analysis - `phplint`
* [ ] Dynamic analysis
  * [X] Unit testing - `phpunit`
  * [X] Coverage - `phpunit --coverage-text`
  * [ ] Functional testing