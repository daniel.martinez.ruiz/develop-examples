#!/usr/bin/python3

import os
import sys
import unittest

if __name__ == '__main__':
    all_tests = unittest.TestLoader().discover('test', pattern='test_*.py')
    result = unittest.TextTestRunner(verbosity=2, buffer=True).run(all_tests)
    os._exit(not result.wasSuccessful())
