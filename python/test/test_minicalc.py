import unittest

from maiarhawkin.devexample.minicalc.minicalc import MiniCalc

class TestMiniCalc(unittest.TestCase):

    def setUp(self):
        self.minicalc = MiniCalc()

    def test_factorial(self):
        with self.assertRaises(ValueError):
            self.minicalc.factorial(-1)
        self.assertEqual(self.minicalc.factorial(0), 1)
        self.assertEqual(self.minicalc.factorial(1), 1)
        self.assertEqual(self.minicalc.factorial(2), 2)
        self.assertEqual(self.minicalc.factorial(3), 6)
        self.assertEqual(self.minicalc.factorial(5), 120)
        self.assertEqual(self.minicalc.factorial(10), 3628800)

    def test_gcd2(self):
        self.assertEqual(self.minicalc.gcd2(0, 0), 0)
        self.assertEqual(self.minicalc.gcd2(1, 1), 1)
        self.assertEqual(self.minicalc.gcd2(2, 2), 2)
        self.assertEqual(self.minicalc.gcd2(2, 3), 1)
        self.assertEqual(self.minicalc.gcd2(2, 4), 2)
        self.assertEqual(self.minicalc.gcd2(36, 60), 12)

    def test_gcd(self):
        self.assertEqual(self.minicalc.gcd([ ]), 0)
        self.assertEqual(self.minicalc.gcd([ 2, 3, 4, 5, 6 ]), 1)
        self.assertEqual(self.minicalc.gcd([ 2, 4, 6, 8, 10 ]), 2)
