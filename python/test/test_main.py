import unittest
from unittest import mock

import sys
from io import StringIO

from maiarhawkin.devexample.main import main

class TestMain(unittest.TestCase):

    def test_main(self):
        with self.assertRaises(SystemExit) as exc:
            main()
        self.assertEqual(exc.exception.code, 2)

        with mock.patch('sys.argv', ['prog']):
            with self.assertRaises(SystemExit) as exc:
                main()
            self.assertEqual(exc.exception.code, 2)

        with mock.patch('sys.argv', ['prog', 'factorial', '-h']):
            with self.assertRaises(SystemExit) as exc:
                main()
            self.assertEqual(exc.exception.code, 0)

        with mock.patch('sys.argv', ['prog', 'factorial', '--help']):
            with self.assertRaises(SystemExit) as exc:
                main()
            self.assertEqual(exc.exception.code, 0)

        with mock.patch('sys.argv', ['prog', 'factorial']):
            with self.assertRaises(SystemExit) as exc:
                main()
            self.assertEqual(exc.exception.code, 2)

        with mock.patch('sys.argv', ['prog', 'gcd']):
            with self.assertRaises(SystemExit) as exc:
                main()
            self.assertEqual(exc.exception.code, 2)

        with mock.patch('sys.argv', ['prog', 'other', '1']):
            with self.assertRaises(SystemExit) as exc:
                main()
            self.assertEqual(exc.exception.code, 2)

        with mock.patch('sys.argv', ['prog', 'factorial', '-1']):
            with self.assertRaises(SystemExit) as exc:
                main()
            self.assertEqual(exc.exception.code, 1)

        with mock.patch('sys.stdout', new=StringIO()) as myoutput:
            with mock.patch('sys.argv', ['prog', 'factorial', '1']):
                main()
                self.assertEqual(myoutput.getvalue().strip(), '1')

        with mock.patch('sys.stdout', new=StringIO()) as myoutput:
            with mock.patch('sys.argv', ['prog', 'factorial', '3']):
                main()
                self.assertEqual(myoutput.getvalue().strip(), '6')

        with mock.patch('sys.stdout', new=StringIO()) as myoutput:
            with mock.patch('sys.argv', ['prog', 'gcd', '1']):
                main()
                self.assertEqual(myoutput.getvalue().strip(), '1')

        with mock.patch('sys.stdout', new=StringIO()) as myoutput:
            with mock.patch('sys.argv', ['prog', 'gcd', '1', '2']):
                main()
                self.assertEqual(myoutput.getvalue().strip(), '1')

        with mock.patch('sys.stdout', new=StringIO()) as myoutput:
            with mock.patch('sys.argv', ['prog', 'gcd', '24', '18']):
                main()
                self.assertEqual(myoutput.getvalue().strip(), '6')
