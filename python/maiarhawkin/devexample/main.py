#!/usr/bin/python3
"""
    CLI interface to run minicalculator operations.
"""
import sys
import argparse

from maiarhawkin.devexample.minicalc.minicalc import MiniCalc

def main():
    """
        Main function.
    """

    parser = argparse.ArgumentParser(description='Maiarhawkin devexample minicalculator.')
    parser.add_argument('operation', type=str, choices=['factorial', 'gcd'],
                        help='operation')
    parser.add_argument('integers', type=int, metavar='num', nargs='+',
                        help='numbers to operate with')
    args = parser.parse_args()

    try:
        minicalc = MiniCalc()
        if args.operation == 'factorial':
            result = minicalc.factorial(args.integers[0])

        elif args.operation == 'gcd':
            result = minicalc.gcd(args.integers)

        print(result)

    except ValueError:
        print('Invalid argument numbers')
        sys.exit(1)


if __name__ == "__main__":                  # pragma: no cover
    main()
