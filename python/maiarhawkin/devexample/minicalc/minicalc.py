"""
    Minicalculator module.
"""

class MiniCalc():
    """
        Minicalculator class.
    """

    def factorial(self, num):
        """
            Factorial function.
        """

        if num < 0:
            raise ValueError

        if num < 2:
            return 1

        return num * self.factorial(num - 1)


    def gcd2(self, num_a, num_b):
        """
            Greater Common Divisor of 2 numbers.
        """

        if num_b == 0:
            return num_a

        return self.gcd2(num_b, num_a % num_b)


    def gcd(self, arr):
        """
            Greater Common Divisor of all numbers in an array.
        """

        if not arr:
            return 0

        left = arr[0]
        for i in arr[1:]:
            left = self.gcd2(left, i)

        return left
