from setuptools import setup, find_packages
import unittest

def allTests():
    return unittest.TestLoader().discover('test', pattern='test_*.py')

setup(
    name = 'develop-examples',
    version = '0.1',
    description = 'Development examples',
    url = 'https://gitlab.com/daniel.martinez.ruiz/develop-examples',
    author = 'Daniel Martinez Ruiz',
    author_email = '',
    license = 'MIT',
    packages = find_packages(exclude=["test"]),
    install_requires = [
    ],
    extras_require={
        'dev': [
            'coverage >= 4.5.3',
            'pylint >= 2.3.1',
            'astroid == 2.2.5',
            # isort above 4.3.4 introduces the "__init__.py not found" issue
            'isort == 4.3.4',
        ]
    },
    entry_points={
        'console_scripts': [
            'maiarhawkin-devexample-main = maiarhawkin.devexample.main:main'
        ]
    },
    zip_safe = False,
    test_suite = 'setup.allTests',
)
