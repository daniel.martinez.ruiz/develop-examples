# Python

## Steps on CI

* [ ] Format
* [ ] Build
* [X] Static analysis - `pylint`
* [ ] Dynamic analysis
  * [X] Unit testing - `unittest`
  * [X] Coverage - `coverage`
  * [ ] Functional testing

## Develop install

```bash
python3 setup.py develop --user
```
