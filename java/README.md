# Java

## Steps on CI

* [ ] Format - `Checkstyle`
* [ ] Build - `javac`
* [X] Static analysis - `PMD`
* [ ] Dynamic analysis
  * [X] Unit testing - `junit & mockito - mvn test`
  * [X] Coverage - `jacoco`
  * [ ] Functional testing
