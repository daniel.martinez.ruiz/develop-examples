package com.maiarhawkin.devexample;

import org.junit.Test;
import org.junit.Before;
import org.junit.Assert;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.*;

import com.maiarhawkin.devexample.DevExample;

/**
 * Unit test for DevExample.
 */
public class DevExampleTest
{
    private DevExample devexample;

    final String PROGRAM_NAME = "dev-example_test";
    final String PARAM_HELP = "-h";
    final String PARAM_HELP2 = "--help";
    final String OP_FACTORIAL = "factorial";
    final String OP_GCD = "gcd";
    final String OP_OTHER = "other";
    final String VALUE__1 = "-1";
    final String VALUE_1 = "1";
    final String VALUE_2 = "2";
    final String VALUE_3 = "3";
    final String VALUE_18 = "18";
    final String VALUE_24 = "24";

    @Before
    public void SetUp() {
        IMiniCalc minicalc = mock(IMiniCalc.class);
        when(minicalc.factorial(-1)).thenThrow(new IllegalArgumentException("received negative value"));
        when(minicalc.factorial(1)).thenReturn(1);
        when(minicalc.factorial(3)).thenReturn(6);
        int[] mock0 = { 1 };
        when(minicalc.gcd(mock0)).thenReturn(1);
        int[] mock1 = { 1, 2 };
        when(minicalc.gcd(mock1)).thenReturn(1);
        int[] mock2 = { 24, 18 };
        when(minicalc.gcd(mock2)).thenReturn(6);

        devexample = new DevExample(minicalc);
    }

    /**
     * Run tests
     */
    @Test
    public void TestDevExampleRun()
    {
        final ByteArrayOutputStream testStdout = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testStdout));

        final String[] test0 = { PROGRAM_NAME };
        Assert.assertEquals(devexample.run(test0), 2);
        testStdout.reset();

        final String[] test1 = { PROGRAM_NAME, OP_FACTORIAL, PARAM_HELP };
        Assert.assertEquals(devexample.run(test1), 1);
        testStdout.reset();

        final String[] test1_1 = { PROGRAM_NAME, OP_FACTORIAL, PARAM_HELP2 };
        Assert.assertEquals(devexample.run(test1_1), 1);
        testStdout.reset();

        final String[] test2 = { PROGRAM_NAME, OP_FACTORIAL };
        Assert.assertEquals(devexample.run(test2), 2);
        testStdout.reset();

        final String[] test3 = { PROGRAM_NAME, OP_GCD };
        Assert.assertEquals(devexample.run(test3), 2);
        testStdout.reset();

        final String[] test3_2 = { PROGRAM_NAME, OP_OTHER, VALUE_1 };
        Assert.assertEquals(devexample.run(test3_2), 2);
        testStdout.reset();

        final String[] test4 = { PROGRAM_NAME, OP_FACTORIAL, VALUE__1 };
        Assert.assertEquals(devexample.run(test4), 1);
        testStdout.reset();

        final String[] test5 = { PROGRAM_NAME, OP_FACTORIAL, VALUE_1 };
        Assert.assertEquals(devexample.run(test5), 0);
        Assert.assertEquals(Integer.parseInt(testStdout.toString().trim()), 1);
        testStdout.reset();

        final String[] test6 = { PROGRAM_NAME, OP_FACTORIAL, VALUE_3 };
        Assert.assertEquals(devexample.run(test6), 0);
        Assert.assertEquals(Integer.parseInt(testStdout.toString().trim()), 6);
        testStdout.reset();

        final String[] test7 = { PROGRAM_NAME, OP_GCD, VALUE_1 };
        Assert.assertEquals(devexample.run(test7), 0);
        Assert.assertEquals(Integer.parseInt(testStdout.toString().trim()), 1);
        testStdout.reset();

        final String[] test8 = { PROGRAM_NAME, OP_GCD, VALUE_1, VALUE_2 };
        Assert.assertEquals(devexample.run(test8), 0);
        Assert.assertEquals(Integer.parseInt(testStdout.toString().trim()), 1);
        testStdout.reset();

        final String[] test9 = { PROGRAM_NAME, OP_GCD, VALUE_24, VALUE_18 };
        Assert.assertEquals(devexample.run(test9), 0);
        Assert.assertEquals(Integer.parseInt(testStdout.toString().trim()), 6);
        testStdout.reset();

    }

}
