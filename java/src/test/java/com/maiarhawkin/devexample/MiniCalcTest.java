package com.maiarhawkin.devexample;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for simple MiniCalc.
 */
public class MiniCalcTest
{
    private final MiniCalc minicalc = new MiniCalc();

    /**
     * Factorial tests
     */
    @Test(expected = IllegalArgumentException.class)
    public void TestMiniCalcFactorialException()
    {
        minicalc.factorial(-1);
    }

    /**
     * Factorial tests
     */
    @Test
    public void TestMiniCalcFactorial()
    {
        Assert.assertEquals(minicalc.factorial(0), 1);
        Assert.assertEquals(minicalc.factorial(1), 1);
        Assert.assertEquals(minicalc.factorial(2), 2);
        Assert.assertEquals(minicalc.factorial(3), 6);
        Assert.assertEquals(minicalc.factorial(5), 120);
        Assert.assertEquals(minicalc.factorial(10), 3628800);
    }

    /**
     * Gcd2 tests
     */
    @Test
    public void TestMiniCalcGcd2()
    {
        Assert.assertEquals(minicalc.gcd2(0, 0), 0);
        Assert.assertEquals(minicalc.gcd2(1, 1), 1);
        Assert.assertEquals(minicalc.gcd2(2, 2), 2);
        Assert.assertEquals(minicalc.gcd2(2, 3), 1);
        Assert.assertEquals(minicalc.gcd2(2, 4), 2);
        Assert.assertEquals(minicalc.gcd2(36, 60), 12);
    }

    /**
     * Gcd tests
     */
    @Test
    public void TestMiniCalcGcd()
    {
        int[] test1 = {};
        Assert.assertEquals(minicalc.gcd(test1), 0);

        int[] test2 = { 2, 3, 4, 5, 6 };
        Assert.assertEquals(minicalc.gcd(test2), 1);

        int[] test3 = { 2, 4, 6, 8, 10 };
        Assert.assertEquals(minicalc.gcd(test3), 2);
    }

}
