package com.maiarhawkin.devexample;

/**
 * MiniCalc
 *
 */
public class MiniCalc implements IMiniCalc
{
    public int factorial(int num)
    {
        if (num < 0)
        {
            throw new IllegalArgumentException("received negative value");
        }
        else if (num < 2)
        {
            return 1;
        }
        else
        {
            return num * factorial(num - 1);
        }
    }

    public int gcd2(int num_a, int num_b)
    {
        if (num_b == 0)
        {
            return num_a;
        }
        else
        {
            return gcd2(num_b, num_a % num_b);
        }
    }

    public int gcd(int[] arr)
    {
        if (arr.length == 0)
        {
            return 0;
        }
        else
        {
            int left = arr[0];
            for (int i = 1; i < arr.length; ++i)
            {
                left = gcd2(left, arr[i]);
            }

            return left;
        }
    }
}
