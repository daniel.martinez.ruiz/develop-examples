package com.maiarhawkin.devexample;

public interface IMiniCalc
{
    abstract public int factorial(int num);
    abstract public int gcd2(int num_a, int num_b);
    abstract public int gcd(int[] arr);
}
