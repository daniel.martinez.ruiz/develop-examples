package com.maiarhawkin.devexample;

import com.maiarhawkin.devexample.DevExample;

public class App
{
    public static void main(String[] args)
    {
        DevExample devexample = new DevExample();
        System.exit(devexample.run(args));
    }
}
