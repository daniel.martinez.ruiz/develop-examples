package com.maiarhawkin.devexample;

/**
 * DevExample
 *
 */
public class DevExample
{
    private IMiniCalc minicalc;

    DevExample()
    {
        minicalc = new MiniCalc();
    }

    DevExample(IMiniCalc m)
    {
        minicalc = m;
    }

    void usage(String name)
    {
        System.out.println("usage: " + name + " [-h] {factorial,gcd} num [num ...]");
        System.out.println();
        System.out.println("Maiarhawkin devexample minicalculator.");
        System.out.println();
        System.out.println("positional arguments:");
        System.out.println("  {factorial,gcd}  operation");
        System.out.println("  num              numbers to operate with");
        System.out.println();
        System.out.println("optional arguments:");
        System.out.println("  -h, --help       show this help message and exit");
    }

    public int run(String[] args)
    {
        final String program_name = args[0];
        if (args.length < 3)
        {
            usage(program_name);
            return 2;
        }

        for (int i = 1; i < args.length; ++i)
        {
            String arg = args[i];
            if ((arg == "-h") || (arg == "--help"))
            {
                usage(program_name);
                return 1;
            }
        }

        try
        {
            int result = 0;
            String operation = args[1];
            if (operation == "factorial")
            {
                result = minicalc.factorial(Integer.parseInt(args[2]));
            }
            else if (operation == "gcd")
            {
                int[] nums = new int[args.length - 2];
                for (int i = 2; i < args.length; ++i)
                {
                    nums[i - 2] = Integer.parseInt(args[i]);
                }
                result = minicalc.gcd(nums);
            }
            else
            {
                usage(program_name);
                return 2;
            }

            System.out.println(result);

        }
        catch (IllegalArgumentException err)
        {
            System.out.println("Invalid argument numbers");
            return 1;
        }

        return 0;
    }
}