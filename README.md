# develop-examples

[![pipeline status](https://gitlab.com/daniel.martinez.ruiz/develop-examples/badges/master/pipeline.svg)](https://gitlab.com/daniel.martinez.ruiz/develop-examples/commits/master)

Examples in different languages with the complete develop cycle.

## Features used

The code is the same on all languages, and as it supports, I am using OOP. The main object is `minicalc` that has 3 maths functions:

* Factorial: Calculate the factorial of a number.
* Gcd2: Calculate the Greatest Common Divosor of 2 number.
* Gcd: Calculate the GCD of a list of numbers.

Over this object, there is a wrapper that takes the parameters from CLI and call the function required.

All the languages are implemented using TDD and has 2 test group. One for the `minicalc` object, and other for the wrapper, mocking the minicalc. The wrapper print the results to stdout, so the buffer has to be mocked also. Also one of the `minicalc` object returns an exception when the arguments are negative, so this has to be managed by the wrapper and the tests.

## Steps on CI

* [ ] Format: On developer device. Must be installed as hook.
* [ ] Build
* [ ] Static analysis
* [ ] Dynamic analysis
  * [ ] Unit testing
  * [ ] Coverage
  * [ ] Functional testing

## Enjoy it
